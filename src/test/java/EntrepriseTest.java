import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EntrepriseTest {

    @Test
    void ajouter() {
        Entreprise testent=new Entreprise("testent");
        Employe testemp=new Employe("test",2500,testent);
        assertEquals(1,testent.getlist().size());
    }
    @Test
    void verifier() {
        Entreprise testent=new Entreprise("testent");
        Employe testemp=new Employe("test",2500,testent);
        assertTrue(testent.verifier(testemp));
    }

    @Test
    void toStringEntreprise() {
        Entreprise testent=new Entreprise("testent");
        Employe testemp=new Employe("test",2500,testent);

        String affiche="nom de l'entreprise "+testent.getnom() + "\n";
        String employes = "Ses employees sont: "+"\n";
        for (Employe employe : testent.getlist()
        ) {
            employes +=employe.getNom() + "\n";
        }
        affiche += employes;
        assertEquals(affiche,testent.toStringEntreprise());
    }
}