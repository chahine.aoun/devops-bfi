import static org.junit.jupiter.api.Assertions.*;

class EmployeTest {

    @org.junit.jupiter.api.Test
    void setNom() {
        Entreprise testent=new Entreprise("testent");
        Employe test=new Employe("test",2500,testent);
        test.setNom("ahmed");
        assertEquals("ahmed",test.getNom());
    }

    @org.junit.jupiter.api.Test
    void setSalaire() {
        Entreprise testent=new Entreprise("testent");
        Employe test=new Employe("test",2500,testent);
        test.setSalaire(3500);
        assertEquals(3500,test.getSalaire());
    }

    @org.junit.jupiter.api.Test
    void setEntreprise() {
        Entreprise testent=new Entreprise("testent");
        Entreprise testent2=new Entreprise("testent2");
        Employe test=new Employe("test",2500,testent);
        test.setEntreprise(testent2);
        assertTrue(testent2.verifier(test) && (testent.verifier(test)));
    }
}
