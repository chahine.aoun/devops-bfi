public class EmployeException extends Exception{
    private Employe employe;
    public EmployeException(Employe employe) {
        this.employe = employe;
    }
    public EmployeException(String message, Employe employe) {
        super(message);
        this.employe = employe;
    }
    public EmployeException(String message, Throwable cause, Employe employe) {
        super(message, cause);
        this.employe = employe;
    }
    public EmployeException(Throwable cause, Employe employe) {
        super(cause);
        this.employe = employe;
    }
    public EmployeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Employe employe) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.employe = employe;
    }
    public EmployeException(String s) {
        super(s);
    }
}
