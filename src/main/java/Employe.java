public class Employe {
    String nom;
    double salaire;
    Entreprise entreprise;
    public Employe(String nom, double salaire, Entreprise entreprise) {
        this.nom = nom;
        this.salaire = salaire;
        this.entreprise = entreprise;
        entreprise.ajouter(this);
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }
    public void setEntreprise(Entreprise entreprise){
        this.entreprise = entreprise;
        entreprise.ajouter(this);
    }
    public String getNom() {
        return nom;
    }
    public double getSalaire() {
        return salaire;
    }
    public Entreprise getEntreprise() {
        return entreprise;
    }
}
