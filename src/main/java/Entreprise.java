import java.util.ArrayList;

public class Entreprise {
    String nom;
    ArrayList<Employe> employes;
    public Entreprise(String nom) {
        this.nom = nom;
        this.employes = new ArrayList<Employe>();
    }
    public String getnom(){
        return this.nom;
    }
    public ArrayList<Employe> getlist(){
        return this.employes;
    }
    void ajouter(Employe employe){
        employes.add(employe);
    }
    public Boolean verifier(Employe employe){
        return employes.contains(employe);
    }
    String toStringEntreprise(){
        String affiche="nom de l'entreprise "+this.nom + "\n";
        String employes = "Ses employees sont: "+"\n";
        for (Employe employe : this.employes
        ) {
            employes +=employe.getNom() + "\n";
        }
        return affiche + employes;
    }
}
